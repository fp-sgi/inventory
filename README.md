# Inventory

* [Online version](http://fp-sgi.s3-website.eu-central-1.amazonaws.com/Inventory/)


## Scope

Creates an inventory of our research data:

* Log files
* Screen recordings
* Camera recordings
* ELAN annotation exports


## Installation

```sh
bundle
```


## Usage

**Important:** Pass the location of the SGI data directory to any of the executables using the environment variable `ROOT`.


### Interactive shell

```sh
./bin/console /path/to/SGI
```


### Starting the web app

```sh
rackup SGI_ROOT=/path/to/SGI
```

### Converting the web pages to static HTML

```sh
./bin/mirror SGI_ROOT=/path/to/SGI
```
