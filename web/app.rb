#!/usr/bin/env ruby
require 'bundler'
Bundler.require :default, :web
require 'sinatra/reloader' if development?

require_relative '../lib/inventory'

configure do
  root     = ARGV.shift || ENV['SGI_ROOT'] || './SGI/'
  $storage = Inventory::Storage.new root
end

get '/' do
  render :haml, :index
end

get '/Files' do
  @storage = $storage
  render :haml, :files
end

get '/Conditions' do
  @conditions = $storage.sessions.inject({}){ |conditions, session|
    session.level_ids_from_logfile.each do |c|
      conditions[c] = 0 unless conditions[c]
      conditions[c] = conditions[c] + 1
    end
    conditions
  }.sort
  render :haml, :conditions
end

get '/Sessions' do
  @sessions = $storage.sessions
  render :haml, :sessions
end

get '/Sessions/:id' do
  @session = $storage.sessions.find{ |s| s.id == params[:id].to_i }
  @levels  = SgiLogParser::Analysis::Level.find @session.log.parser
  render :haml, :session
end

get '/Annotations' do
  @tag_files = $storage.tag_files.sort
  render :haml, :annotations
end

get '/Annotations/:level' do
  @level     = params[:level]
  @tag_files = $storage.tag_files.select{|tf| tf.level_id == @level}.sort
  render :haml, :annotations_for_level
end

get '/Exports' do
  @tables = Inventory::Export::Table.tables
  render :haml, :exports
end

get '/Exports/:klass.tsv' do
  @table = Inventory::Export::Table.tables(params[:klass]).new($storage)
  content_type 'application/csv'
  attachment   "#{@table.class_name}.tsv"
  @table.to_csv col_sep: "\t", write_headers: true
end

get '/Exports/:klass' do
  @table = Inventory::Export::Table.tables(params[:klass]).new($storage)
  render :haml, :export
end
