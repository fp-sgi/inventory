class Array
   def to_ranges
      compact.sort.uniq.inject([]) do |r,x|
         r.empty? || r.last.last.succ != x ? r << (x..x) : r[0..-2] << (r.last.first..x)
      end
   end
end

class Range
  alias_method :original_to_s, :to_s

  def to_s
    self.count == 1 ? self.first.to_s : "#{self.first} – #{self.last}"
  end
end
