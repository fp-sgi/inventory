module Inventory
  class Storage
    def initialize(data_dir)
      @log_expr = data_dir + 'Logs/*.{csj,log}'
      @scr_expr = data_dir + 'Screencasts/Metadata/*.json'
      @cam_expr = data_dir + 'Videokamera/Metadata/*.json'
      @tag_expr = data_dir + 'ELAN/Annotations/*/*.txt'
    end

    def sessions
      @sessions ||= log_files.map { |log|
        id     = log.session_id
        screen = screen_videos.find{ |v| v.session_id == id }
        camera = camera_videos.find{ |v| v.session_id == id }
        tags   = tag_files.select{   |t| t.session_id == id }
        Session.new(log, screen, camera, tags) if screen && camera
      }.compact.sort
    end

    def log_files
      @log_files     ||= Dir[@log_expr].map{ |p| Logfile.new(p) }
    end

    def screen_videos
      @screen_videos ||= Dir[@scr_expr].map{ |p| ScreenVideo.new(p) }
    end

    def camera_videos
      @camera_videos ||= Dir[@cam_expr].map{ |p| CameraVideo.new(p) }
    end

    def tag_files
      @tag_files     ||= Dir[@tag_expr].map{ |p| Tagfile.new(p) }.sort
    end
  end
end
