require 'json'

module Inventory

  class Video < Artifact
    def file_start_time
      json = JSON.parse File.read(path)
      str  = json[0]['DateTimeOriginal'] || json[0]['MediaCreateDate']
      date = DateTime.strptime(str, '%Y:%m:%d %H:%M:%S')
      date + Rational(device_offset, 86400)
    end
  end

  class ScreenVideo < Video
    def device_offset
      + 2 * 60 * 60  # Time zone or something like that?
    end
  end

  class CameraVideo < Video
    def device_offset
      - 935
    end
  end

end
