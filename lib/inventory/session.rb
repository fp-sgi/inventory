module Inventory
  class Session
    include Enumerable
    attr_accessor :inventory, :id, :log, :screen, :camera, :tagfiles

    def initialize(log, screen, camera, tagfiles)
      @log, @screen, @camera, @tagfiles = log, screen, camera, tagfiles
      [ @log, @screen, @camera ].each{ |artifact| artifact.session = self }
      @tagfiles.each{ |tagfile| tagfile.session = self }
    end

    def id
      @id ||= @log.session_id
    end

    def <=>(other)
      self.id <=> other.id
    end

    def level_ids_from_tagfiles
      tagfiles.map &:level_id
    end

    def level_ids_from_logfile
      log.level_ids
    end

    def computed_conditions(total = 4)
      bits = id.to_s(2)
      bits = ('0' * total + bits)[bits.length, total].split('')
      computed_conditions, letter = [], 0
      until bits.empty?
        computed_conditions << "#{(letter + 65).chr}#{bits.pop == '0' ? 'C' : 'T'}"
        letter += 1
      end
      computed_conditions
    end

    # FIXME: Hardcoded tutorial levels =/
    def computed_level_ids(total = 4, levels_per_condition = 2)
      @computed_level_ids ||= %w(T1C T2C T3C T4C T5C) + computed_conditions(total).map{ |condition|
        (1..levels_per_condition).map{ |i|
          "#{condition[0,1]}#{i}#{condition[-1,1]}"
        }
      }.flatten
    end

  end
end
