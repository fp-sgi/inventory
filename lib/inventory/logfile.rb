module Inventory

  class Logfile < Artifact
    def file_start_time
      value = @filename.match(/\d+_([\d\-_]+)/)[1]
      value = DateTime.strptime(value, '%d-%m-%Y_%H-%M-%S')
      value + Rational(device_offset, 86400)
    end

    def level_ids
      events.map(&:level_id).compact.uniq
    end

    def levels
      SgiLogParser::Analysis::Level.find(parser)
    end
  end

end
