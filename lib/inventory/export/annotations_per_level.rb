require 'set'

module Inventory::Export
  class AnnotationsPerLevel < Table

    export 'Annotations per level'

    def headers
      [
        :level_id,
        :level_number,
        :level_condition,
        :level_duration,
        :annotation_name,
        :annotation_count,
        :annotation_count_per_sec,
        :annotation_duration,
        :annotation_duration_per_sec,
      ]
    end

    def data
      lines = []

      files.group_by(&:level_id).each{ |level_id, elan_files|
        level_data = {
          level_id:        elan_files.first.level_id,
          level_number:    elan_files.first.level_number,
          level_condition: elan_files.first.level_condition,
          level_duration:  elan_files.first.level_duration
        }

        duration_reference = elan_files.first.level_duration * player_number

        tags.each do |tag|
          count = elan_files.inject(0){ |sum, file|
            sum + file.annotations.select{ |a| a.text == tag }.count
          }
          duration = elan_files.inject(0){ |sum, file|
            sum + file.annotations.select{ |a| a.text == tag }.map(&:duration).sum
          }
          tag_data = {
            annotation_name:             tag,
            annotation_count:            count,
            annotation_count_per_sec:    count / duration_reference,
            annotation_duration:         duration,
            annotation_duration_per_sec: duration / duration_reference,
          }
          lines << level_data.merge(tag_data)
        end
      }

      lines
    end

  private

    def player_number
      2
    end

    def tags
      @tags ||= files.inject(Set.new){ |tags, file| tags += file.annotations.map(&:text) }.sort
    end

    def files
      storage.tag_files
    end

  end
end
