require 'set'

module Inventory::Export
  class EventsPerSession < Table

    export 'Events per session'

    def headers
      [
        :session_id,
        :level_id,
        :level_number,
        :level_condition,
        :level_duration,
        :event_name,
        :event_count,
        :event_count_per_sec,
      ]
    end

    def data
      lines = []

      sessions.each do |session|
        session.log.levels.each do |level|
          level_data = {
            session_id:      session.id,
            level_id:        level.level_id,
            level_number:    level.level_id[0...-1],
            level_condition: level.level_id[-1, 1],
            level_duration:  level.duration
          }

          duration_reference = level.duration * player_number

          known_event_names.each do |event_name|
            events = level.events.select{ |a| a.name == event_name }
            count  = events.count

            event_data = {
              event_name:          event_name,
              event_count:         count,
              event_count_per_sec: count / duration_reference,
            }
            lines << level_data.merge(event_data)
          end

        end
      end

      lines
    end

  private

    def player_number
      2
    end

    def known_event_names
      @known_event_names ||= begin
        events = sessions.inject(Set.new){ |events, session|
          events += session.log.events.map(&:name)
        }
        events -= %w(LevelStart LevelEnd SessionStart SessionEnd)
        events.sort
      end
    end

    def sessions
      storage.sessions
    end

  end
end
