require 'set'

module Inventory::Export
  class PlayerGoals < Table

    export 'Waiting times until level end'

    def headers
      [
        :session_id,
        :player_number,
        :level_id,
        :level_number,
        :level_condition,
        :level_duration,
        :first_goal_time,
        :waiting_time,
      ]
    end

    def data
      lines = []

      sessions.each do |session|
        session.log.levels.each do |level|
          level_data = {
            session_id:      session.id,
            level_id:        level.level_id,
            level_number:    level.level_id[0...-1],
            level_condition: level.level_id[-1, 1],
            level_duration:  level.duration
          }

          (1..player_count).each do |player_number|
            next unless first_goal_reached = level.events.find{ |e|
              e.params.player == "#{player_number}" &&
              e.name == 'GoalReached'
            }
            player_data = {
              player_number: player_number,
              first_goal_time: (first_goal_reached.time - level.start_time).round(4),
              waiting_time:    (level.end_time - first_goal_reached.time).round(4),
            }
            lines << level_data.merge(player_data)
          end
        end
      end

      lines
    end

  private

    def player_count
      2
    end

    def sessions
      storage.sessions
    end

  end
end
