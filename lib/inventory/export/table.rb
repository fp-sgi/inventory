require 'csv'

module Inventory
  module Export

    class Table
      attr_reader :storage
      def initialize(storage)
        @storage = storage
      end

      def headers
        data.first.keys
      end

      def data
        raise('Not implemented, please overwrite `data` method.')
      end

      def description
        self.class.description
      end

      def class_name
        self.class.class_name
      end

      def self.class_name
        self.name.split('::').last
      end

      def self.description
        @description
      end

      def to_csv(csv_options = {})
        csv_options.merge!({headers: headers.map(&:to_s)})
        CSV.generate(csv_options) do |csv|
          data.each do |row|
            csv << headers.map{ |h| row[h] }
          end
        end
      end

    protected

      def self.export(table_name = nil)
        @description = table_name ||= self.class_name
        @@tables ||= {}
        @@tables[@description] = self
      end

      def self.tables(class_name = nil)
        result = @@tables || {}
        if class_name
          result.find{ |_, klass| klass.class_name == class_name}[1]
        else
          result
        end
      end
    end

  end
end
