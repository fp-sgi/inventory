require 'set'

module Inventory::Export
  class Players < Table

    export 'Players'

    def headers
      [
        :session_id,
        :player_number,
        :player_id,
      ] +
      known_level_numbers.map{ |level_number|
        [
          "#{level_number}_condition",
          "#{level_number}_duration",
        ] +
        known_tags.map{ |tag|
          prefix = "#{level_number}_#{tag}"
          [
            "#{level_number}_#{tag}_count",
            "#{level_number}_#{tag}_duration",
          ]
        }
      }.flatten.map(&:to_sym)
    end

    def data
      lines = []

      sessions.each do |session|
        (1..player_count).each do |player_number|
          player_data = {
            session_id:    session.id,
            player_number: player_number,
            player_id:     "#{session.id}_#{player_number}"
          }

          tag_data = {}
          known_level_numbers.each{ |level_number|
            # Take the first ELAN export from the session you can find with the
            # correct level number, do not care about the condition tested
            tagfile = session.tagfiles.find{ |f| f.level_number == level_number }
            next unless tagfile

            player_annotations = tagfile.annotations.select{ |a|
              a.tier == "Player#{player_number}"
            }

            tag_data.merge!({
              "#{level_number}_condition": tagfile.level_condition,
              "#{level_number}_duration":  tagfile.level_duration,
            })

            known_tags.each{ |tag|
              annotations = player_annotations.select{ |a| a.text == tag }

              tag_data.merge!({
                "#{level_number}_#{tag}_count":     annotations.count,
                "#{level_number}_#{tag}_duration":  annotations.map(&:duration).sum,
              })
            }
          }

          lines << tag_data.merge(player_data)
        end
      end

      lines
    end

  private

    def player_count
      2
    end

    def known_level_numbers
      @known_level_numbers ||= sessions.inject([]) { |level_numbers, session|
        level_numbers += session.tagfiles.map(&:level_number)
      }.uniq.sort
    end

    def known_tags
      @known_tags ||= sessions.inject(Set.new) { |session_tags, session|
        session_tags += session.tagfiles.inject(Set.new) { |file_tags, tagfile|
          file_tags += tagfile.annotations.map(&:text)
        }
      }.sort
    end

    def sessions
      storage.sessions
    end

  end
end
