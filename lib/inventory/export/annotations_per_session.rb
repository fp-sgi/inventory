require 'set'

module Inventory::Export
  class AnnotationsPerSession < Table

    export 'Annotations per session'

    def headers
      [
        :session_id,
        :level_id,
        :level_number,
        :level_condition,
        :level_duration,
        :annotation_name,
        :annotation_count,
        :annotation_count_per_sec,
        :annotation_duration,
        :annotation_duration_per_sec,
      ]
    end

    def data
      lines = []

      files.each{ |elan_file|
        session_data = {
          session_id:      elan_file.session_id,
          level_id:        elan_file.level_id,
          level_number:    elan_file.level_number,
          level_condition: elan_file.level_condition,
          level_duration:  elan_file.level_duration
        }

        duration_reference = elan_file.level_duration * player_number

        tags.each do |tag|
          annotations = elan_file.annotations.select{ |a| a.text == tag }
          count    = annotations.count
          duration = annotations.map(&:duration).sum

          tag_data = {
            annotation_name:             tag,
            annotation_count:            count,
            annotation_count_per_sec:    count / duration_reference,
            annotation_duration:         duration,
            annotation_duration_per_sec: duration / duration_reference,
          }
          lines << session_data.merge(tag_data)
        end
      }

      lines
    end

  private

    def player_number
      2
    end

    def tags
      @tags ||= files.inject(Set.new){ |tags, file| tags += file.annotations.map(&:text) }.sort
    end

    def files
      storage.tag_files
    end

  end
end
