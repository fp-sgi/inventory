require 'set'

module Inventory::Export
  class PlayerGoalsWide < Table

    export 'Waiting times until level end (wide)'

    def headers
      [
        :session_id,
      ] +
      known_level_numbers.map{ |level_number|
        [
          "#{level_number}_condition",
          "#{level_number}_duration",
        ] +
        (1..player_count).map{ |player_number|
          [
            "#{level_number}_player#{player_number}_first_goal_time",
            "#{level_number}_player#{player_number}_waiting_time",
          ]
        }
      }.flatten.map(&:to_sym)
    end

    def data
      lines = []

      sessions.each do |session|

        session_data = { session_id: session.id, }

        session.log.levels.each do |level|
          session_data.merge!({
            "#{level.level_number}_condition": level.level_condition,
            "#{level.level_number}_duration":  level.duration,
          })

          (1..player_count).each do |player_number|
            next unless first_goal_reached = level.events.find{ |e|
              e.params.player == "#{player_number}" &&
              e.name == 'GoalReached'
            }
            session_data.merge!({
              "#{level.level_number}_player#{player_number}_first_goal_time": \
                (first_goal_reached.time - level.start_time).round(4),
              "#{level.level_number}_player#{player_number}_waiting_time": \
                (level.end_time - first_goal_reached.time).round(4),
            })
          end
        end

        lines << session_data
      end

      lines
    end

  private

    def known_level_numbers
      @known_level_numbers ||= sessions.inject([]) { |level_numbers, session|
        level_numbers += session.level_ids_from_logfile.map{|id| id[0...-1]}
      }.uniq.sort
    end

    def player_count
      2
    end

    def sessions
      storage.sessions
    end

  end
end
