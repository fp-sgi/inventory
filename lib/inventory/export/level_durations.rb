module Inventory::Export
  class LevelDurations < Table

    export 'Game level durations'

    def headers
      [ :level_id, :level_number, :level_condition, :duration ]
    end

    def data
      storage.tag_files.map { |elan|
        {
          level_id:        elan.level_id,
          level_number:    elan.level_number,
          level_condition: elan.level_condition,
          duration:        elan.level_duration
        }
      }
    end

  end
end
