module Inventory

  class Tagfile
    attr_reader   :level_id, :session_id, :path, :filename
    attr_accessor :session

    def initialize(path)
      @path       = path
      @filename   = File.basename(path, '.*')
      level_substr   = filename.match(/(.{3})_/)[1]
      session_substr = filename.match(/_(\d+)/)[1]
      @level_id   = level_substr   ? level_substr        : raise("No level id found in #{filename}")
      @session_id = session_substr ? session_substr.to_i : raise("No session id found in #{filename}")
    end

    def to_s
      "level #{@level_id}, session #{@session_id}"
    end

    def <=>(other)
      [@level_id, @session_id] <=> [other.level_id, other.session_id]
    end

    def level_number
      @level_id[0...-1]
    end

    def level_condition
      @level_id[-1, 1]
    end

    def level_duration
      parser.level_duration
    end

    def annotations
      parser.split_annotations
    end

    # Sum up annotation durations in seconds
    def annotations_total
      @annotations_total ||= annotations.inject(0){ |sum, a| sum + a.duration }
    end

    # Return portion of level covered by annotations (between 0 and 1)
    def annotations_coverage
      annotations_total / (level_duration * 2)
    end

    def parser
      @parser ||= SgiElanParser::Parser.open path
    end
  end

end
