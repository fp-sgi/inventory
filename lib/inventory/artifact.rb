class Inventory::Artifact
  attr_reader   :session_id, :path, :filename
  attr_accessor :session

  def initialize(path)
    @path       = path
    @filename   = File.basename(path, '.*')
    id          = filename.match(/(\d+)/)[1]
    @session_id = id ? id.to_i : raise("No session id found in #{filename}")
  end

  def file_start_time
    raise('Artifact#file_start_time not implemented')
  end

  def device_offset
    0
  end

  def cache_key
    "#{self.class.to_s.gsub(/^.*::/, '')}_#{self.session_id}"
  end

  def events
    parser.events
  end

  def parser
    @parser ||= SgiLogParser::Parser.open path
  end
end
